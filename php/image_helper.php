<?php
class ImageHelper
{
    private $info;

    public function __construct($info)
    {
        $this->info = $info;
    }

    /**
     * base64形式のソースコードから画像をアップロード
     * @param array $base64s
     * @param array $imgNames
     */
    public function base64ImgUpload($base64s, $imgNames)
    {
        $base64s = explode(",", $base64s);
        $imgNames = explode(",", $imgNames);
        for ($i = 0; $i < count($base64s); $i++) {
            $imgNames[$i] = urldecode($imgNames[$i]);

            if ($this->info['env'] == true) {
                $imgNames[$i] = $_SERVER['DOCUMENT_ROOT'] . '/upload/' . '/'. $imgNames[$i];
            }
            else {
                $imgNames[$i] = $_SERVER['DOCUMENT_ROOT'] . '/upload/' . '/' . $imgNames[$i];
            }
            
            // BASE64バイナリ文字列をファイルに変換して書き出す
            $base64s[$i] = base64_decode($base64s[$i]);
            $base64s[$i] = preg_replace("/data:[^,]+,/i", "", $base64s[$i]);
            $base64s[$i] = base64_decode($base64s[$i]);
            file_put_contents(mb_convert_encoding($imgNames[$i], 'UTF-8', 'ASCII, JIS, UTF-8, SJIS'), $base64s[$i]);
        }
    }

     /**
     * フォームからPOSTされた画像をアップロード
     * @param array $postImg
     */
    public function imgUpload($postImg)
    {
        
        if ($this->info['env'] == true) {
            $imgUpFolder = $_SERVER['DOCUMENT_ROOT'] . '/upload/' . '/';
        }
        else {
            $imgUpFolder = $_SERVER['DOCUMENT_ROOT'] .  '/';
        }
        
        $img1Name = $postImg['name'];
        $img1Tmp = $postImg['tmp_name'];
        $img1Size = $postImg['size'];
        $img1Ext = strtolower(pathinfo($img1Name, PATHINFO_EXTENSION)); // get image extension
        $finalImg1 = $_SESSION['account']['shop_id'] . '_' . date("YmdHis") . '.' . $img1Ext;
        
        
        if (isset($postImg) && $postImg != '') {
            if ($postImg['error'] > 0) {
                // die('アップロード中にエラーが発生しました');
            }
            
            if (!getimagesize($img1Tmp)) {
                die('画像ファイル（jpg/gif/png）をアップロードしてください');
            }
            
            // Check filetype
            $valid_extensions = array('jpeg', 'jpg', 'png', 'gif');
            if (! (in_array($img1Ext, $valid_extensions))) {
                die('サポートされていない画像形式です');
            }

            // Check filesize
            if ($img1Size > 5000000) {
                die('ファイルアップロードの上限サイズを超えています');
            }

            // Check if the file exists
            if (file_exists('upload/' . $finalImg1)) {
                die('同じファイル名の画像が既にアップロードされています');
            }

            // Upload file
            if (!move_uploaded_file($img1Tmp, $imgUpFolder . $finalImg1)) {
                die('申し訳ございません。ただいまアップロードができない状態です。時間がたっても治らない場合はお問い合わせください。');
            }
        }
        return $finalImg1;

    }

/**
 * 画像から平均色を取得
 * @param string $filepath
 */
    function detectImgColor($filepath)
    {
        $ext = mb_strtolower(substr($filepath, strrpos($filepath, '.') + 1));

            //画像(今回はPNG)をオブジェクトとして読み込んで
            if($ext=='jpg' || $ext=='jpeg'){
                $img  = imagecreatefromjpeg($filepath);
            }else if($ext=='png'){
                $img  = imagecreatefrompng($filepath);
            }
            $imgX = imagesx($img); //ヨコと
            $imgY = imagesy($img); //タテのpx数を取得して
            $imgXY = $imgX*$imgY; //掛けあわせて
            $rSum = '';
            $gSum = '';
            $bSum = '';
            for ($y = 0; $y < $imgY; $y++) { //左上から右下にかけてfor文で走査
                for ($x = 0; $x < $imgX; $x++) {
                    $rgb = imagecolorat($img, $x, $y); //rgbコードを取得して
                    $r = ($rgb >> 16) & 0xFF; //赤を10進数に
                    $g = ($rgb >> 8) & 0xFF; //緑を10進数に
                    $b = $rgb & 0xFF; //青を10進数に
                    $rSum += $r; //それぞれを合算していく...
                    $gSum += $g;
                    $bSum += $b;
                }
            }
            //合算された赤(R),緑(G),青(B)をそれぞれ画像の合計px数で割り、
            //再度16進数に変換した上で出力
            return '#'.dechex($rSum/$imgXY).dechex($gSum/$imgXY).dechex($bSum/$imgXY);
        
    }
}

