<?php

/**
 * サニタイズ
 * @param string $str
 */
function h($str)
{
    $str = htmlspecialchars($str, ENT_QUOTES, 'UTF-8');

    $str = str_replace('&lt;br&gt;', '<br>', $str);
    $str = str_replace('&lt;div&gt;', '<div>', $str);
    $str = str_replace('&lt;/div&gt;', '</div>', $str);
    // $str = str_replace('&lt;img class="img_in_&gt', '<br>', $str);
    // $str = strip_tags($str,'<div><br><img>');
    // $str = str_replace('on', '<br>', $a);

    return $str;
}

/**
 * 前後にある半角全角スペースを削除する関数
 * @param string $str
 */
function spaceTrim($str)
{
    // 行頭
    $str = preg_replace('/^[ 　]+/u', '', $str);
    // 末尾
    $str = preg_replace('/[ 　]+$/u', '', $str);
    return $str;
}

/**
 * 変数のダンプ
 * @param array $arg
 */
function d($arg)
{
    foreach($arg as $value){
        echo "<pre>";
        var_dump($value);
        echo "</pre>";
    }

    exit();
}

/**
 * 文字を丸める
 * @param string $string
 * @param int $limit
 * @param string $trimMarker
 */
function cut($string,$limit,$trimMarker){
    return mb_strimwidth($string, 0, $limit, $trimMarker);
}

/**
 * 画像と文字の混ざったテキストをHTML用にフォーマット
 * @param string $body 
 * @param string $shopId 
 * @param array $info
 */
function toImageAndString($body,$on,$shop_id,$info)
{
    $body_array = explode("[.+.]", $body);
    $img_extensions = array('jpeg', '.jpg', '.png', '.gif', 'png');
    $fixed_body = '';
    foreach ($body_array as &$value)
        {
        if (in_array(mb_strtolower(mb_substr($value, -4)), $img_extensions, true)) {
            $value = '<img class="img_in_'.$on.'" src="' . $info['PATH']['UPLOAD'] . $shop_id . '/' . $value . '" alt="'.$value.'">';
        }
        $fixed_body .= $value;
    }
    unset($value);

    return $fixed_body;
}

/**
 * 引数で指定された月の、日と曜日を取得
 * @param int $year
 * @param int $month
 */
function getCalendar($year, $month)
{
    // 月末日を取得
    $lastDate = date('j', mktime(0, 0, 0, $month + 1, 0, $year));
    $calendar = array();
    $calendar['year'] = $year;
    $calendar['month'] = $month;
    $j = 0;
        
    // 月末日までループ
    for ($i = 1; $i < $lastDate + 1; $i++) {
        // 曜日を取得
        $week = date('w', mktime(0, 0, 0, $month, $i, $year));
        // 1日の場合
        if ($i == 1) {
            // 1日目の曜日までをループ
            for ($s = 1; $s <= $week; $s++) {
                // 前半に空文字をセット
                $calendar['array'][$j]['day'] = '';
                $calendar['array'][$j]['week'] = $week;
                $calendar['array'][$j]['date'] = '';
                
                $j++;
            }
        }
 
        // 配列に日付をセット
        // $calendar['array'][$j]['date'] = str_pad($i, 2, 0, STR_PAD_LEFT);
        $calendar['array'][$j]['day'] = $i;
        $calendar['array'][$j]['week'] = $week;
        $calendar['array'][$j]['date'] = $year .'-' . $month .'-' .sprintf("%02s", $i);
                    // $calendar[$j]['body'] = '';
        $j++;
         // 月末日の場合
        if ($i == $lastDate) {
            // 月末日から残りをループ
            for ($e = 1; $e <= 6 - $week; $e++) {
                //後半に空文字をセット
                $calendar['array'][$j]['day'] = '';
                $calendar['array'][$j]['week'] = $week;
                $calendar['array'][$j]['date'] = '';
                $j++;
            }
        }
    }
    return $calendar;
}

/**
 * アクセスがアプリのウェブビューからかどうか判別
 * @param string $ua
 */
function isApp($ua)
{
    if ($ua == 'android_miseapp_user' || $ua == 'android_miseapp_client' || $ua == 'ios_miseapp_user' || $ua == 'ios_miseapp_client')
        {
        return true;
    }
    else {
        return false;
    }
}

/**
 * アクセスがPCからかどうか判別
 * @param string $ua
 */
function isPc($ua)
{
    if ( (strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false) || (strpos($ua, 'iPhone') !== false) || (strpos($ua, 'Windows Phone') !== false)) {
        //スマホからアクセスされた場合
        return false;
    }
    elseif ( (strpos($ua, 'Android') !== false) || (strpos($ua, 'iPad') !== false)) {
        // タブレットからアクセスされた場合
        return false;
    }
    else {
        return true;
    }
}

/**
 * アクセスされたデバイスを判別
 * @param string $ua
 */
function whichDevice($ua){
    if ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false) || (strpos($ua, 'iPhone') !== false) || (strpos($ua, 'Windows Phone') !== false)) {
        //スマホからアクセスされた場合
        return 'sp';
    
    } elseif ((strpos($ua, 'Android') !== false) || (strpos($ua, 'iPad') !== false)) {
        // タブレットからアクセスされた場合
        return 'tablet';
    
    }
    else {
        return 'pc';
    }
}

function isAblePage($info, $config, $actionName)
{

    $exceptionList = [
        'registPushTopicApi',
        'setting'
    ];
    
    if (!in_array($actionName, $exceptionList, true)) {
        $actionList = [
            [
                'action' => 'usertop',
                'flag' => 'menuTopFlag'
            ],
            [
                'action' => 'menu',
                'flag' => 'menuMenuFlag'
            ],
            [
                'action' => 'gallery',
                'flag' => 'menuGalleryFlag'
            ],
            [
                'action' => 'kijilist',
                'flag' => 'menuBlogFlag'
            ],
            [
                'action' => 'kiji',
                'flag' => 'menuBlogFlag'
            ],
            [
                'action' => 'info',
                'flag' => 'menuInfoFlag'
            ],
            [
                'action' => 'coupon',
                'flag' => 'menuCouponFlag'
            ],
            [
                'action' => 'reserve',
                'flag' => 'menuReserveFlag'
            ],
            [
                'action' => 'calendar',
                'flag' => 'menuCalendarFlag'
            ],
            [
                'action' => 'access',
                'flag' => 'menuAccessFlag'
            ],
        ];

        $flagName = array_column($actionList, "flag", "action")[$actionName];
        if ($config[$flagName] == 0) {
            return false;
        }

        return true;
    }
}

/**
 * csrf対策
 * @param object viewHelper
 */
function csrfCheck($viewHelper)
{
    if (isset($_POST['csrfToken']) && $_POST['csrfToken'] != '') {
        if ($_POST['csrfToken'] != $_SESSION['csrfToken']) {
            return false;
        }
    }
    $csrfToken = base64_encode(openssl_random_pseudo_bytes(32));
    $_SESSION['csrfToken'] = $csrfToken;
    $viewHelper->assign('csrfToken', $csrfToken);
}

/**
 * curl関数を使いやすくしたもの
 * @param string $url
 * @param string $ua
 */
function curlUrl($url,$ua){
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_HEADER, false );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
    curl_setopt( $ch, CURLOPT_USERAGENT,$ua);
    $result = curl_exec( $ch );
    curl_close( $ch );
    echo $result;
}

/**
 * プッシュ通知をトピックに送信
 * @param string $apiKey
 * @param string $topic
 * @param string $message
 */
function sendPush($apiKey, $topic, $message)
{
    $data = [
        "notification" => [
                "title" => $message['title'],
                "body" =>$message['body'],
        ],
        "to" => '/topics/'.$topic,
        "priority" => "high",
        "content_available" => true
    ];
    $jsonData = json_encode($data);
    $url = "https://fcm.googleapis.com/fcm/send";
    $headers = array(
        "Content-Type: application/json",
        "Authorization: key=".$apiKey
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $html = curl_exec($ch);
    curl_close($ch);
}

/**
 * トークンをトピックに登録
 * @param 
 */
function registIdToTopic($apiKey, $topic, $registrationId)
{
    $url = "https://iid.googleapis.com/iid/v1/" . $registrationId . "/rel/topics/" . $topic;
    $headers = array(
        "Content-Type: application/json",
        "Content-Length: 0",
        "Authorization: key=" . $apiKey
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $html = curl_exec($ch);
    curl_close($ch); 
}

/**
 * 色味調整
 * @param string $hex
 * @param int $degree
 * @param string $mode
 */
function fixColor($hex, $degree, $mode = 'normal')
{

    if ($mode == 'normal') {
        $rgb = hexToRgb($hex);
        $out = '#';
        foreach ($rgb as &$value) {
            $value = $value + $degree;
            if ((0 > $value || 255 < $value)) {
            $value = (0 > $value) ? 0 : 255;
            }
        }
        unset($value);
        $out = '#' . rgbTohex($rgb);

    } elseif ($mode == 'complement') {//補色
        $rgb = hexToRgb($hex);

        $rgb_max = max($rgb['r'], $rgb['g'], $rgb['b']);
        $rgb_min = min($rgb['r'], $rgb['g'], $rgb['b']);
        $cc = $rgb_min + $rgb_max;
        $rgb['r'] = $cc - $rgb['r'];
        $rgb['g'] = $cc - $rgb['g'];
        $rgb['b'] = $cc - $rgb['b'];

        $out = '#' . rgbTohex($rgb);

    } elseif ($mode == 'invert') {//反転色
        $hex = (strpos($hex, '#') === 0) ? substr($hex, 1) : $hex;
        $hex = strtoupper($hex);
        if (strlen($hex) == 3) {
            $r = substr($hex, 0, 1);
            $g = substr($hex, 1, 1);
            $b = substr($hex, 2, 1);
            $hex = $r . $r . $g . $g . $b . $b;
        }

        $a = '0123456789ABCDEF';
        $b = 'FEDCBA9876543210';
        $out = '#';
        foreach (str_split($hex) as $v) {
            $out .= $b {
                strpos($a, $v)};
        }
    }
        
    return $out;
}

/**
 * t_reserveテーブルのoenChoiceカラムの中身を配列にする
 * @param string $arg
 */
function formatOwnChoice($arg){

    $ownChoice =[];
    $ownChoiceData=explode('[■]', $arg);
    array_pop($ownChoiceData);
    foreach($ownChoiceData as $key => $value){
        $oneownChoice=explode('[.+.]', $value);
        $ownChoice[$key]['title'] = $oneownChoice[0];
        $ownChoice[$key]['flag'] = $oneownChoice[1];
        $oneOwnOption = explode('._.', $oneownChoice[2]);
        array_pop($oneOwnOption);
        $ownChoice[$key]['option'] = $oneOwnOption;
    };
    return $ownChoice;
}

/**
 * ショップメニューをフォーマット
 * @param array $shopMenu
 */
function formatShopMenu($shopMenu){
    foreach ($shopMenu as &$oneShopMenu) {
        $itemsData = explode('[■]', $oneShopMenu['item']);
        array_pop($itemsData);
        foreach ($itemsData as $key => $oneItemData) {
            $oneItem = explode('[.+.]', $oneItemData);

            $oneShopMenu['formatItems'][$key]['name'] = $oneItem[0];
            $oneShopMenu['formatItems'][$key]['price'] = $oneItem[1];
        }
    }
        unset($oneShopMenu);

        return $shopMenu;
}

/**
 * 背景色から、読みやすい文字色を判別
 * @param string $hex
 */
function judgeTextColor($hex){
    $rgb = hexToRgb($hex);
     
        $y = 0.299 * $rgb['r'] + 0.587 * $rgb['g'] + 0.114 * $rgb['b'];

     
        $textColor = ($y > 127) ? '#000000' : '#ffffff';
return $textColor;
}

/**
 * rgb形式の色を16進数に変換
 * @param array $rgb
 */
function rgbToHex($rgb)
{
    $hex = '';
    foreach ($rgb as $value) {
        
        $hex .= sprintf('%02s', dechex($value));
    }
    return $hex;

}

/**
 * 16進数のカラーコードをrgb形式（配列）に変換
 * @param string $hex
 */
function hexToRgb($hex)
{
    $hex = str_replace("#", "", $hex);

    if (strlen($hex) == 3) {
        $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
        $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
        $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
    } else {
        $r = hexdec(substr($hex, 0, 2));
        $g = hexdec(substr($hex, 2, 2));
        $b = hexdec(substr($hex, 4, 2));
    }
    $rgb = [
        'r'=>$r,
        'g'=>$g,
        'b'=>$b,
    ];
   
    return $rgb;
}

/**
 * 16進数のカラーコードをrgb形式（文字列）に変換
 * @param 
 */
function hexToRgbString($hex)
{
    $hex = str_replace("#", "", $hex);

    if (strlen($hex) == 3) {
        $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
        $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
        $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
    } else {
        $r = hexdec(substr($hex, 0, 2));
        $g = hexdec(substr($hex, 2, 2));
        $b = hexdec(substr($hex, 4, 2));
    }
    $rgb = $r.','. $g.','. $b;
    return $rgb;
}







